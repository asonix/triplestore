mod disowned;
mod iri;
mod langtag;
mod object;
mod revisioned;
mod subject;

use bonsaidb::core::{
    document::{BorrowedDocument, Emit},
    key::Key,
    schema::{
        view::map::Mappings, Collection, CollectionMapReduce, MapReduce, Schema, View, ViewSchema,
    },
};

pub(crate) use self::{
    disowned::Disowned,
    iri::{Iri, OwnedIri},
    langtag::{LanguageTag, OwnedLanguageTag},
    object::Object,
    revisioned::Revisioned,
    subject::Subject,
};

#[derive(Debug, Schema)]
#[schema(name = "TriplestoreSchema", collections = [TripleRecord, IriRecord, StringRecord, LanguageTagRecord, DisownedRecord], authority = "asonix/triplestore")]
pub(crate) struct TriplestoreSchema;

// Collections

/// A collection that stores the relationships of RDF Triples
#[derive(Clone, Collection, Debug, serde::Deserialize, serde::Serialize)]
#[collection(name = "TripleRecord", authority = "asonix/triplestore", views = [
    TriplesByPredicate,
    TriplesBySubjectAndPredicate,
    TriplesBySubjectPredicateAndObject,
    TriplesBySubjectIri,
    TriplesByIri,
    TriplesByString,
    TriplesByLangtag,
])]
pub(crate) struct TripleRecord {
    pub(crate) subject: Subject,
    pub(crate) predicate: u128,
    pub(crate) object: Object,
}

#[derive(Clone, Collection, Debug, Key)]
#[collection(name = "IriRecord", authority = "asonix/triplestore", primary_key = u128, views = [IriUniqueness], serialization = Key)]
pub(crate) struct IriRecord {
    #[key]
    pub(crate) iri: Revisioned<OwnedIri>,
}

impl IriRecord {
    pub(crate) fn new(iri: iref::IriBuf) -> Self {
        Self {
            iri: Revisioned::new(iri.into()),
        }
    }
}

#[derive(Clone, Collection, Debug, Key)]
#[collection(name = "StringRecord", authority = "asonix/triplestore", primary_key = u128, views = [StringUniqueness], serialization = Key)]
pub(crate) struct StringRecord {
    #[key]
    pub(crate) string: Revisioned<String>,
}

impl StringRecord {
    pub(crate) fn new(string: String) -> Self {
        Self {
            string: Revisioned::new(string),
        }
    }
}

#[derive(Clone, Collection, Debug, Key)]
#[collection(name = "LanguageTagRecord", authority = "asonix/triplestore", primary_key = u128, views = [LanguageTagUniqueness], serialization = Key)]
pub(crate) struct LanguageTagRecord {
    #[key]
    pub(crate) langtag: Revisioned<OwnedLanguageTag>,
}

impl LanguageTagRecord {
    pub(crate) fn new(language_tag: ::langtag::LanguageTagBuf) -> Self {
        Self {
            langtag: Revisioned::new(language_tag.into()),
        }
    }
}

#[derive(Clone, Collection, Debug, Key)]
#[collection(name = "DisownedRecord", authority = "asonix/triplestore", views = [], serialization = Key)]
pub(crate) struct DisownedRecord {
    #[key]
    pub(crate) disowned: Disowned,
}

impl DisownedRecord {
    pub(crate) fn iri(iri: u128) -> Self {
        Self {
            disowned: Disowned::Iri(iri),
        }
    }

    pub(crate) fn string(string: u128) -> Self {
        Self {
            disowned: Disowned::String(string),
        }
    }

    pub(crate) fn langtag(langtag: u128) -> Self {
        Self {
            disowned: Disowned::Langtag(langtag),
        }
    }
}

// Views

// TripleRecord Views

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = u128)]
pub(crate) struct TriplesByPredicate;

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = (Subject, u128))]
pub(crate) struct TriplesBySubjectAndPredicate;

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = (Subject, u128, Object))]
#[view_schema(policy = Unique)]
pub(crate) struct TriplesBySubjectPredicateAndObject;

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = u128)]
pub(crate) struct TriplesBySubjectIri;

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = u128)]
pub(crate) struct TriplesByIri;

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = u128)]
pub(crate) struct TriplesByString;

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = TripleRecord, key = u128)]
pub(crate) struct TriplesByLangtag;

// IriRecord views

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = IriRecord, key = OwnedIri)]
#[view_schema(mapped_key = Iri<'doc>, policy = Unique)]
pub(crate) struct IriUniqueness;

// StringRecord views

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = StringRecord, key = String)]
#[view_schema(mapped_key = std::borrow::Cow<'doc, str>, policy = Unique)]
pub(crate) struct StringUniqueness;

// LanguageTagRecord views

#[derive(Clone, Debug, View, ViewSchema)]
#[view(collection = LanguageTagRecord, key = OwnedLanguageTag)]
#[view_schema(mapped_key = LanguageTag<'doc>, policy = Unique)]
pub(crate) struct LanguageTagUniqueness;

// MapReduces

// TripleRecord MapReduces

impl CollectionMapReduce for TriplesByPredicate {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        document.header.emit_key(document.contents.predicate)
    }
}

impl CollectionMapReduce for TriplesBySubjectAndPredicate {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        document
            .header
            .emit_key((document.contents.subject, document.contents.predicate))
    }
}

impl CollectionMapReduce for TriplesBySubjectPredicateAndObject {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        document.header.emit_key((
            document.contents.subject,
            document.contents.predicate,
            document.contents.object,
        ))
    }
}

impl CollectionMapReduce for TriplesBySubjectIri {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        let iri = match document.contents.subject {
            Subject::Iri(iri) | Subject::Blank { iri, .. } => iri,
        };

        document.header.emit_key(iri)
    }
}

impl CollectionMapReduce for TriplesByIri {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        let mappings = match document.contents.subject {
            Subject::Iri(iri) | Subject::Blank { iri, .. } => document.header.emit_key(iri)?,
        };

        let mappings = mappings.and(document.header.emit_key(document.contents.predicate)?);

        match document.contents.object {
            Object::Iri(iri) | Object::Blank { iri, .. } | Object::TypedString { iri, .. } => {
                Ok(mappings.and(document.header.emit_key(iri)?))
            }
            Object::String(_) | Object::LangString { .. } => Ok(mappings),
        }
    }
}

impl CollectionMapReduce for TriplesByString {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        match document.contents.object {
            Object::TypedString { string, .. }
            | Object::String(string)
            | Object::LangString { string, .. } => document.header.emit_key(string),
            Object::Blank { .. } | Object::Iri(_) => Ok(Mappings::none()),
        }
    }
}

impl CollectionMapReduce for TriplesByLangtag {
    fn map<'doc>(
        &self,
        document: bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self>
    where
        bonsaidb::core::document::CollectionDocument<<Self::View as View>::Collection>: 'doc,
    {
        match document.contents.object {
            Object::LangString { langtag, .. } => document.header.emit_key(langtag),
            Object::String(_)
            | Object::TypedString { .. }
            | Object::Blank { .. }
            | Object::Iri(_) => Ok(Mappings::none()),
        }
    }
}

// IriRecord MapReduces

impl MapReduce for IriUniqueness {
    fn map<'doc>(
        &self,
        document: &'doc BorrowedDocument<'_>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self> {
        let bytes =
            bonsaidb::core::key::ByteSource::Borrowed(revisioned::key_slice(&document.contents));

        // This works since Iri is Key and transparent over OwnedIri, which is
        // transparent over Iri
        let iri = Iri::from_ord_bytes(bytes).expect("IRI is valid format");

        document.header.emit_key_and_value(iri, ())
    }
}

// StringRecord MapReduces

impl MapReduce for StringUniqueness {
    fn map<'doc>(
        &self,
        document: &'doc BorrowedDocument<'doc>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self> {
        let str = std::str::from_utf8(revisioned::key_slice(&document.contents))
            .expect("IRI is valid format");

        document
            .header
            .emit_key_and_value(std::borrow::Cow::Borrowed(str), ())
    }
}

// LanguageTagRecord MapReduces

impl MapReduce for LanguageTagUniqueness {
    fn map<'doc>(
        &self,
        document: &'doc BorrowedDocument<'_>,
    ) -> bonsaidb::core::schema::ViewMapResult<'doc, Self> {
        let bytes =
            bonsaidb::core::key::ByteSource::Borrowed(revisioned::key_slice(&document.contents));

        // This works since LangaugeTag is Key and transparent over OwnedLanguageTag, which is
        // transparent over LanguageTag
        let langtag = LanguageTag::from_ord_bytes(bytes).expect("langtag is valid format");

        document.header.emit_key_and_value(langtag, ())
    }
}
