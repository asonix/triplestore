mod api;
mod schema;

use bonsaidb::{
    core::connection::AsyncStorageConnection,
    local::config::Builder,
    server::{CustomServer, NoBackend, ServerConfiguration},
};
use iref::IriBuf;

use api::Triple;
use rdf_types::BlankIdBuf;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let server = CustomServer::open(
        ServerConfiguration::new_with_backend("triplestore.bonsaidb", NoBackend)
            .with_api::<api::TripleHandler, api::InsertDocument>()?
            .with_schema::<schema::TriplestoreSchema>()?,
    )
    .await?;

    server
        .create_database::<schema::TriplestoreSchema>("triplestore", true)
        .await?;

    api::insert_document(
        &server,
        vec![
            Triple {
                subject: IriBuf::new("https://example.com")
                    .expect("valid IRI")
                    .into(),
                predicate: IriBuf::new("https://example.com")
                    .expect("valid IRI")
                    .into(),
                object: IriBuf::new("https://example.com")
                    .expect("valid IRI")
                    .into(),
            },
            Triple {
                subject: IriBuf::new("https://yiff.life/users/6my")
                    .expect("valid IRI")
                    .into(),
                predicate: IriBuf::new("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                    .expect("valid IRI")
                    .into(),
                object: IriBuf::new("https://www.w3.org/ns/activitystreams#Person")
                    .expect("valid IRI")
                    .into(),
            },
            Triple {
                subject: IriBuf::new("https://yiff.life/users/6my")
                    .expect("valid IRI")
                    .into(),
                predicate: IriBuf::new("http://www.w3.org/ns/ldp#inbox")
                    .expect("valid IRI")
                    .into(),
                object: IriBuf::new("https://yiff.life/users/6MY/inbox")
                    .expect("valid IRI")
                    .into(),
            },
            Triple {
                subject: IriBuf::new("https://yiff.life/users/6my")
                    .expect("valid IRI")
                    .into(),
                predicate: IriBuf::new("https://www.w3.org/ns/activitystreams#attachment")
                    .expect("valid IRI")
                    .into(),
                object: BlankIdBuf::new(String::from("_:b0"))
                    .expect("valid blank node id")
                    .into(),
            },
            Triple {
                subject: BlankIdBuf::new(String::from("_:b0"))
                    .expect("valid blank node id")
                    .into(),
                predicate: IriBuf::new("http://schema.org#value")
                    .expect("valid IRI")
                    .into(),
                object: String::from("he/him").into(),
            },
            Triple {
                subject: BlankIdBuf::new(String::from("_:b0"))
                    .expect("valid blank node id")
                    .into(),
                predicate: IriBuf::new("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
                    .expect("valid IRI")
                    .into(),
                object: IriBuf::new("http://schema.org#PropertyValue")
                    .expect("valid IRI")
                    .into(),
            },
            Triple {
                subject: BlankIdBuf::new(String::from("_:b0"))
                    .expect("valid blank node id")
                    .into(),
                predicate: IriBuf::new("https://www.w3.org/ns/activitystreams#name")
                    .expect("valid IRI")
                    .into(),
                object: String::from("Pronouns").into(),
            },
        ],
    )
    .await?;

    println!("Hello, world!");
    Ok(())
}
