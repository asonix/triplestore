use serde::de::Error as _;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Subject(pub(super) rdf_types::Subject);

impl From<rdf_types::BlankIdBuf> for Subject {
    fn from(value: rdf_types::BlankIdBuf) -> Self {
        Self(rdf_types::Subject::Blank(value))
    }
}

impl From<iref::IriBuf> for Subject {
    fn from(value: iref::IriBuf) -> Self {
        Self(rdf_types::Subject::Iri(value))
    }
}

impl From<rdf_types::Subject> for Subject {
    fn from(value: rdf_types::Subject) -> Self {
        Self(value)
    }
}

impl From<crate::schema::IriRecord> for Subject {
    fn from(value: crate::schema::IriRecord) -> Self {
        Self(rdf_types::Subject::Iri(value.iri.key.into()))
    }
}

impl serde::Serialize for Subject {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let string = match &self.0 {
            rdf_types::Id::Iri(iri) => format!("iri:{}", iri.as_str()),
            rdf_types::Id::Blank(blank_id) => format!("blk:{}", blank_id.as_str()),
        };

        serializer.serialize_str(&string)
    }
}

impl<'de> serde::Deserialize<'de> for Subject {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let string = String::deserialize(deserializer)?;

        let subject = match &string[0..4] {
            "iri:" => {
                let iri_buf = iref::IriBuf::from_string(String::from(&string[4..]))
                    .map_err(|(e, _)| D::Error::custom(e))?;

                rdf_types::Subject::Iri(iri_buf)
            }
            "blk:" => {
                let blank_id_buf = rdf_types::BlankIdBuf::new(String::from(&string[4..]))
                    .map_err(|e| D::Error::custom(e.0))?;

                rdf_types::Subject::Blank(blank_id_buf)
            }
            _ => return Err(D::Error::custom("Invalid prefix")),
        };

        Ok(Subject(subject))
    }
}

#[cfg(test)]
mod tests {
    use super::Subject;

    #[test]
    fn can_roundtrip_stored_object() {
        let inputs = [
            Subject(rdf_types::Id::Iri(
                iref::IriBuf::new("https://yiff.life/users/6my").expect("Valid Iri"),
            )),
            Subject(rdf_types::Id::Blank(
                rdf_types::BlankIdBuf::new(String::from("_:a")).expect("Valid Blank ID"),
            )),
        ];

        for input in inputs {
            let bytes = serde_json::to_vec(&input).expect("Serialized successfully");

            let subject: Subject =
                serde_json::from_slice(&bytes).expect("Deserialized successfully");

            assert_eq!(input, subject);
        }
    }
}
