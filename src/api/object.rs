use serde::{de::Error as _, ser::Error as _};

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Object(pub(super) rdf_types::Object);

impl From<(String, langtag::LanguageTagBuf)> for Object {
    fn from(value: (String, langtag::LanguageTagBuf)) -> Self {
        Self(rdf_types::Object::Literal(rdf_types::Literal::LangString(
            value.0, value.1,
        )))
    }
}

impl From<(String, iref::IriBuf)> for Object {
    fn from(value: (String, iref::IriBuf)) -> Self {
        Self(rdf_types::Object::Literal(rdf_types::Literal::TypedString(
            value.0, value.1,
        )))
    }
}

impl From<String> for Object {
    fn from(value: String) -> Self {
        Self(rdf_types::Object::Literal(rdf_types::Literal::String(
            value,
        )))
    }
}

impl From<rdf_types::Literal> for Object {
    fn from(value: rdf_types::Literal) -> Self {
        Self(rdf_types::Object::Literal(value))
    }
}

impl From<rdf_types::BlankIdBuf> for Object {
    fn from(value: rdf_types::BlankIdBuf) -> Self {
        Self(rdf_types::Object::Id(rdf_types::Id::Blank(value)))
    }
}

impl From<iref::IriBuf> for Object {
    fn from(value: iref::IriBuf) -> Self {
        Self(rdf_types::Object::Id(rdf_types::Id::Iri(value)))
    }
}

impl From<rdf_types::Id> for Object {
    fn from(value: rdf_types::Id) -> Self {
        Self(rdf_types::Object::Id(value))
    }
}

impl From<rdf_types::Object> for Object {
    fn from(value: rdf_types::Object) -> Self {
        Self(value)
    }
}

impl From<crate::schema::IriRecord> for Object {
    fn from(value: crate::schema::IriRecord) -> Self {
        Self(rdf_types::Object::Id(rdf_types::Subject::Iri(
            value.iri.key.into(),
        )))
    }
}

impl From<crate::schema::StringRecord> for Object {
    fn from(value: crate::schema::StringRecord) -> Self {
        Self(rdf_types::Object::Literal(rdf_types::Literal::String(
            value.string.key,
        )))
    }
}

impl From<(crate::schema::StringRecord, crate::schema::IriRecord)> for Object {
    fn from((string, iri): (crate::schema::StringRecord, crate::schema::IriRecord)) -> Self {
        Self(rdf_types::Object::Literal(rdf_types::Literal::TypedString(
            string.string.key,
            iri.iri.key.into(),
        )))
    }
}

impl
    From<(
        crate::schema::StringRecord,
        crate::schema::LanguageTagRecord,
    )> for Object
{
    fn from(
        (string, langtag): (
            crate::schema::StringRecord,
            crate::schema::LanguageTagRecord,
        ),
    ) -> Self {
        Self(rdf_types::Object::Literal(rdf_types::Literal::LangString(
            string.string.key,
            langtag.langtag.key.into(),
        )))
    }
}

#[derive(Debug)]
pub(crate) enum ObjectError {
    InvalidPrefix,
    InvalidIri(iref::Error),
    InvalidBlankId(rdf_types::InvalidBlankId<String>),
    MissingTypedStringSeparator,
    InvalidTypedStringLength(std::num::ParseIntError),
    TypedStringLengthOutOfBounds,
    InvalidTypedStringIri(iref::Error),
    MissingLangStringSeparator,
    InvalidLangStringLength(std::num::ParseIntError),
    LangStringLengthOutOfBounds,
    InvalidLanguageTag((langtag::Error, Vec<u8>)),
}

impl serde::Serialize for Object {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().map_err(S::Error::custom)?.as_ref())
    }
}

impl<'de> serde::Deserialize<'de> for Object {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let string = String::deserialize(deserializer)?;

        Self::from_str(&string).map_err(D::Error::custom)
    }
}

impl std::fmt::Display for ObjectError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidPrefix => write!(f, "Prefix for stored string is unknown"),
            Self::InvalidIri(_) => write!(f, "Iri for stored string is invalid"),
            Self::InvalidBlankId(e) => write!(f, "Blank ID is not formatted properly: {}", e.0),
            Self::MissingTypedStringSeparator => {
                write!(f, "Typed String is not formatted properly")
            }
            Self::InvalidTypedStringLength(_) => write!(f, "Typed string length is invalid"),
            Self::TypedStringLengthOutOfBounds => write!(f, "Typed string length is too long"),
            Self::InvalidTypedStringIri(_) => write!(f, "Typed String Iri is invalid"),
            Self::MissingLangStringSeparator => write!(f, "Lang String is not formatted properly"),
            Self::InvalidLangStringLength(_) => write!(f, "Lang string lenght is invalid"),
            Self::LangStringLengthOutOfBounds => write!(f, "Language Tag is missing"),
            Self::InvalidLanguageTag((e, _)) => write!(f, "Language Tag is invalid: {}", e),
        }
    }
}

impl std::error::Error for ObjectError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::InvalidIri(e) => Some(e),
            Self::InvalidTypedStringLength(e) => Some(e),
            Self::InvalidTypedStringIri(e) => Some(e),
            Self::InvalidLangStringLength(e) => Some(e),
            _ => None,
        }
    }
}

impl Object {
    fn to_string(&self) -> Result<String, ObjectError> {
        match &self.0 {
            rdf_types::Term::Id(rdf_types::Id::Iri(iri)) => Ok(format!("iri:{}", iri.as_str())),
            rdf_types::Term::Id(rdf_types::Id::Blank(blank)) => {
                Ok(format!("blk:{}", blank.as_str()))
            }
            rdf_types::Term::Literal(rdf_types::Literal::String(string)) => {
                Ok(format!("str:{string}"))
            }
            rdf_types::Term::Literal(rdf_types::Literal::TypedString(string, iri)) => {
                Ok(format!("tst:{}:{}{}", string.len(), string, iri.as_str()))
            }
            rdf_types::Term::Literal(rdf_types::Literal::LangString(string, language_tag_buf)) => {
                Ok(format!(
                    "lst:{}:{}{}",
                    string.len(),
                    string,
                    language_tag_buf
                ))
            }
        }
    }

    fn from_str(string: &str) -> Result<Self, ObjectError> {
        match &string[0..4] {
            "iri:" => {
                let iri = string.trim_start_matches("iri:");
                let iri_buf = iref::IriBuf::new(iri).map_err(ObjectError::InvalidIri)?;

                Ok(Object(rdf_types::Term::Id(rdf_types::Id::Iri(iri_buf))))
            }
            "blk:" => {
                let blank_id = string.trim_start_matches("blk:");
                let blank_id_buf = rdf_types::BlankIdBuf::new(blank_id.to_owned())
                    .map_err(ObjectError::InvalidBlankId)?;

                Ok(Object(rdf_types::Term::Id(rdf_types::Id::Blank(
                    blank_id_buf,
                ))))
            }
            "str:" => {
                let string = string.trim_start_matches("str:");

                Ok(Object(rdf_types::Term::Literal(
                    rdf_types::Literal::String(string.to_owned()),
                )))
            }
            "tst:" => {
                let string = string.trim_start_matches("tst:");

                let Some((first, rest)) = string.split_once(':') else {
                    return Err(ObjectError::MissingTypedStringSeparator);
                };

                let first_len: usize = first
                    .parse()
                    .map_err(ObjectError::InvalidTypedStringLength)?;

                if first_len > rest.len() {
                    return Err(ObjectError::TypedStringLengthOutOfBounds);
                }

                let (string, iri) = rest.split_at(first_len);

                let iri_buf = iref::IriBuf::new(iri).map_err(ObjectError::InvalidTypedStringIri)?;

                Ok(Object(rdf_types::Term::Literal(
                    rdf_types::Literal::TypedString(string.to_owned(), iri_buf),
                )))
            }
            "lst:" => {
                let string = string.trim_start_matches("lst:");

                let Some((first, rest)) = string.split_once(':') else {
                    return Err(ObjectError::MissingLangStringSeparator);
                };

                let first_len: usize = first
                    .parse()
                    .map_err(ObjectError::InvalidLangStringLength)?;

                if first_len > rest.len() {
                    return Err(ObjectError::LangStringLengthOutOfBounds);
                }

                let (string, langtag) = rest.split_at(first_len);

                let langtag_buf =
                    langtag::LanguageTagBuf::<Vec<u8>>::new(langtag.as_bytes().to_vec())
                        .map_err(ObjectError::InvalidLanguageTag)?;

                Ok(Object(rdf_types::Term::Literal(
                    rdf_types::Literal::LangString(string.to_owned(), langtag_buf),
                )))
            }
            _ => Err(ObjectError::InvalidPrefix),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Object;

    #[test]
    fn can_roundtrip_stored_object() {
        let inputs = [
            Object(rdf_types::Term::Id(rdf_types::Id::Iri(
                iref::IriBuf::new("https://yiff.life/users/6my").expect("Valid Iri"),
            ))),
            Object(rdf_types::Term::Id(rdf_types::Id::Blank(
                rdf_types::BlankIdBuf::new(String::from("_:a")).expect("Valid Blank ID"),
            ))),
            Object(rdf_types::Term::Literal(rdf_types::Literal::String(
                String::from("My Cool String"),
            ))),
            Object(rdf_types::Term::Literal(rdf_types::Literal::TypedString(
                String::from("1"),
                iref::IriBuf::new("https://example.com/ns#Number").expect("Valid Iri"),
            ))),
            Object(rdf_types::Term::Literal(rdf_types::Literal::LangString(
                String::from("Cool, another string!"),
                langtag::LanguageTagBuf::new(Vec::from(&b"en"[..])).expect("Valid Language Tag"),
            ))),
        ];

        for input in inputs {
            let string = input.to_string().expect("Turned into string successfully");

            let stored_object = Object::from_str(&string).expect("Parsed from string successfully");

            assert_eq!(input, stored_object);
        }
    }
}
