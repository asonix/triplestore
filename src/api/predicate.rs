use serde::de::Error as _;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Predicate(pub(super) iref::IriBuf);

impl From<iref::IriBuf> for Predicate {
    fn from(value: iref::IriBuf) -> Self {
        Self(value)
    }
}

impl From<crate::schema::IriRecord> for Predicate {
    fn from(value: crate::schema::IriRecord) -> Self {
        Self(value.iri.key.into())
    }
}

impl serde::Serialize for Predicate {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.0.as_str())
    }
}

impl<'de> serde::Deserialize<'de> for Predicate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let iri_buf = iref::IriBuf::new(&s).map_err(D::Error::custom)?;

        Ok(Self(iri_buf))
    }
}
