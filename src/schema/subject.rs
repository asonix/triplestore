use bonsaidb::core::key::{Key, KeyEncoding};

#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize,
)]
pub(crate) enum Subject {
    Iri(u128),
    Blank { iri: u128, discriminant: u64 },
}

#[derive(Debug, PartialEq, Eq)]
pub(crate) enum SubjectError {
    IncorrectByteLength(usize),
}

impl Subject {
    const SHORT_LEN: usize = 16; // std::mem::size_of::<u128>();
    const LONG_LEN: usize = 24; // std::mem::size_of::<u128>() + std::mem::size_of::<u64>();

    const fn bytes_len(&self) -> usize {
        match self {
            Self::Iri(_) => Self::SHORT_LEN,
            Self::Blank { .. } => Self::LONG_LEN,
        }
    }

    fn to_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::with_capacity(self.bytes_len());

        match self {
            Self::Iri(key) => bytes.extend_from_slice(&key.to_be_bytes()[..]),
            Self::Blank { iri, discriminant } => {
                bytes.extend_from_slice(&iri.to_be_bytes()[..]);
                bytes.extend_from_slice(&discriminant.to_be_bytes()[..]);
            }
        }

        bytes
    }

    fn from_slice(bytes: &[u8]) -> Result<Self, SubjectError> {
        match bytes.len() {
            Self::SHORT_LEN => {
                let key =
                    u128::from_be_bytes(bytes[..16].try_into().expect("Length is already checked"));
                Ok(Self::Iri(key))
            }
            Self::LONG_LEN => {
                let iri =
                    u128::from_be_bytes(bytes[..16].try_into().expect("Length is already checked"));
                let discriminant = u64::from_be_bytes(
                    bytes[16..24].try_into().expect("Length is already checked"),
                );

                Ok(Self::Blank { iri, discriminant })
            }
            size => Err(SubjectError::IncorrectByteLength(size)),
        }
    }
}

impl KeyEncoding for Subject {
    type Error = SubjectError;

    const LENGTH: Option<usize> = None;

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(std::borrow::Cow::Owned(self.to_bytes()))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}

impl<'k> Key<'k> for Subject {
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        Self::from_slice(bytes.as_ref())
    }
}

impl std::fmt::Display for SubjectError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IncorrectByteLength(size) => write!(f, "Incorrect byte length: {size}"),
        }
    }
}

impl std::error::Error for SubjectError {}

#[cfg(test)]
mod tests {
    use super::Subject;

    #[test]
    fn round_trip() {
        let inputs = [
            Subject::Iri(u128::MAX),
            Subject::Iri(0),
            Subject::Iri(99),
            Subject::Blank {
                iri: u128::MAX,
                discriminant: u64::MAX,
            },
            Subject::Blank {
                iri: 0,
                discriminant: u64::MAX,
            },
            Subject::Blank {
                iri: 99,
                discriminant: u64::MAX,
            },
            Subject::Blank {
                iri: u128::MAX,
                discriminant: 0,
            },
            Subject::Blank {
                iri: 0,
                discriminant: 0,
            },
            Subject::Blank {
                iri: 99,
                discriminant: 0,
            },
            Subject::Blank {
                iri: u128::MAX,
                discriminant: 99,
            },
            Subject::Blank {
                iri: 0,
                discriminant: 99,
            },
            Subject::Blank {
                iri: 99,
                discriminant: 99,
            },
        ];

        for input in inputs {
            let bytes = input.to_bytes();

            let subject = Subject::from_slice(&bytes).expect("Deserialize successfully");

            assert_eq!(input, subject);
        }
    }
}
