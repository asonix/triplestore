use bonsaidb::core::key::{Key, KeyEncoding};

pub(super) fn key_slice(revisioned_slice: &[u8]) -> &[u8] {
    &revisioned_slice[16..]
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Revisioned<T> {
    pub(crate) revision: u128,
    pub(crate) key: T,
}

#[derive(Debug)]
pub(crate) enum RevisionedError<E> {
    DecodeKeyError(E),
    EncodeKeyError(E),
    InvalidBytesLength(usize),
}

impl<T> Revisioned<T> {
    pub(crate) fn new(key: T) -> Self {
        Self { revision: 0, key }
    }

    pub(crate) fn increment(&mut self) {
        self.revision += 1;
    }
}

impl<T, U> KeyEncoding<Revisioned<U>> for Revisioned<T>
where
    T: KeyEncoding<U>,
{
    type Error = RevisionedError<T::Error>;

    const LENGTH: Option<usize> = {
        if let Some(len) = T::LENGTH {
            Some(len + 16)
        } else {
            None
        }
    };

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        let bytes = self
            .key
            .as_ord_bytes()
            .map_err(RevisionedError::EncodeKeyError)?;

        let mut output = Vec::with_capacity(bytes.len() + 16);
        output.extend_from_slice(&self.revision.to_be_bytes()[..]);
        output.extend_from_slice(&bytes);

        Ok(std::borrow::Cow::Owned(output))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}

impl<'k, T> Key<'k> for Revisioned<T>
where
    T: Key<'k>,
{
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        if bytes.len() < 16 {
            return Err(RevisionedError::InvalidBytesLength(bytes.len()));
        }

        let revision =
            u128::from_be_bytes(bytes[0..16].try_into().expect("Length already checked"));

        let key = match bytes {
            bonsaidb::core::key::ByteSource::Owned(owned) => T::from_ord_bytes(
                bonsaidb::core::key::ByteSource::Ephemeral(key_slice(&owned)),
            )
            .map_err(RevisionedError::DecodeKeyError)?,
            bonsaidb::core::key::ByteSource::Ephemeral(ephemeral) => T::from_ord_bytes(
                bonsaidb::core::key::ByteSource::Ephemeral(key_slice(ephemeral)),
            )
            .map_err(RevisionedError::DecodeKeyError)?,
            bonsaidb::core::key::ByteSource::Borrowed(borrowed) => T::from_ord_bytes(
                bonsaidb::core::key::ByteSource::Borrowed(key_slice(borrowed)),
            )
            .map_err(RevisionedError::DecodeKeyError)?,
        };

        Ok(Revisioned { revision, key })
    }
}

impl<T> std::fmt::Display for RevisionedError<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::DecodeKeyError(_) => write!(f, "Failed to decode key type"),
            Self::EncodeKeyError(_) => write!(f, "Failed to encode key type"),
            Self::InvalidBytesLength(size) => write!(f, "Invalid length for key bytes: {size}"),
        }
    }
}

impl<T> std::error::Error for RevisionedError<T>
where
    T: std::error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::DecodeKeyError(e) => Some(e),
            Self::EncodeKeyError(e) => Some(e),
            Self::InvalidBytesLength(_) => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use bonsaidb::core::key::{Key, KeyEncoding};

    use super::Revisioned;

    fn do_round_trip<I, T>(inputs: I)
    where
        I: IntoIterator<Item = Revisioned<T>>,
        T: std::fmt::Debug + Eq + KeyEncoding + for<'k> Key<'k>,
    {
        for input in inputs {
            let bytes = input.as_ord_bytes().expect("Serialized ok");

            let revisioned = Revisioned::<T>::from_ord_bytes(bytes.into()).expect("Deserialize ok");

            assert_eq!(input, revisioned);
        }
    }

    #[test]
    fn round_trip_strings() {
        do_round_trip([
            Revisioned::new(String::from("")),
            Revisioned::new(String::from("hi")),
            Revisioned::new(String::from("hello")),
            Revisioned::new(String::from(
                "A really really long like kinda long but reasonably long string",
            )),
        ])
    }

    #[test]
    fn round_trip_numbers() {
        do_round_trip([
            Revisioned::new(0_usize),
            Revisioned::new(usize::MAX),
            Revisioned::new(99),
        ])
    }
}
