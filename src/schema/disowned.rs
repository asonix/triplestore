use bonsaidb::core::key::{Key, KeyEncoding};

#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize,
)]
pub(crate) enum Disowned {
    Iri(u128),
    String(u128),
    Langtag(u128),
}

#[derive(Debug, PartialEq, Eq)]
pub(crate) enum DisownedError {
    IncorrectByteLength(usize),
    InvalidType,
}

impl Disowned {
    const IRI: u8 = 0;
    const STRING: u8 = 1;
    const LANGTAG: u8 = 2;

    const LEN: usize = 17; // std::mem::size_of::<u128>() + 1;

    fn to_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::with_capacity(Self::LEN);

        match self {
            Self::Iri(key) | Self::String(key) | Self::Langtag(key) => {
                bytes.extend_from_slice(&key.to_be_bytes()[..])
            }
        }

        bytes.push(self.type_id());

        bytes
    }

    fn from_slice(bytes: &[u8]) -> Result<Self, DisownedError> {
        match bytes.len() {
            Self::LEN => {
                let key = u128::from_be_bytes(
                    bytes[0..16].try_into().expect("Length is already checked"),
                );

                match bytes[Self::LEN - 1] {
                    Self::IRI => Ok(Self::Iri(key)),
                    Self::STRING => Ok(Self::String(key)),
                    Self::LANGTAG => Ok(Self::Langtag(key)),
                    _ => Err(DisownedError::InvalidType),
                }
            }
            other => Err(DisownedError::IncorrectByteLength(other)),
        }
    }

    const fn type_id(&self) -> u8 {
        match self {
            Self::Iri(_) => Self::IRI,
            Self::String(_) => Self::STRING,
            Self::Langtag(_) => Self::LANGTAG,
        }
    }
}

impl KeyEncoding for Disowned {
    type Error = DisownedError;

    const LENGTH: Option<usize> = Some(Self::LEN);

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(std::borrow::Cow::Owned(self.to_bytes()))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}

impl<'k> Key<'k> for Disowned {
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        Self::from_slice(bytes.as_ref())
    }
}

impl std::fmt::Display for DisownedError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IncorrectByteLength(size) => write!(f, "Incorrect byte length: {size}"),
            Self::InvalidType => write!(f, "Invalid type byte"),
        }
    }
}

impl std::error::Error for DisownedError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::IncorrectByteLength(_) => None,
            Self::InvalidType => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Disowned;

    #[test]
    fn round_trip() {
        let inputs = [
            Disowned::Iri(u128::MAX),
            Disowned::Iri(0),
            Disowned::Iri(99),
            Disowned::String(u128::MAX),
            Disowned::String(0),
            Disowned::String(99),
            Disowned::Langtag(u128::MAX),
            Disowned::Langtag(0),
            Disowned::Langtag(99),
        ];

        for input in inputs {
            let bytes = input.to_bytes();

            let object = Disowned::from_slice(&bytes).expect("Deserialize successfully");

            assert_eq!(input, object);
        }
    }
}
