use bonsaidb::core::key::{Key, KeyEncoding};

#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize,
)]
pub(crate) enum Object {
    Iri(u128),
    Blank { iri: u128, discriminant: u64 },
    String(u128),
    TypedString { string: u128, iri: u128 },
    LangString { string: u128, langtag: u128 },
}

#[derive(Debug, PartialEq, Eq)]
pub(crate) enum ObjectError {
    IncorrectByteLength(usize),
    InvalidType,
}

impl Object {
    const IRI: u8 = 0;
    const BLANK: u8 = 1;
    const STRING: u8 = 2;
    const TYPED_STRING: u8 = 3;
    const LANG_STRING: u8 = 4;

    const SHORT_LEN: usize = 17; // std::mem::size_of::<u128>() + 1;
    const MID_LEN: usize = 25; // std::mem::size_of::<u128>() + std::mem::size_of::<u64>();
    const LONG_LEN: usize = 33; // 2 * std::mem::size_of::<u128>() + 1;

    fn to_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::with_capacity(self.bytes_len());

        match self {
            Self::Iri(key) | Self::String(key) => {
                bytes.extend_from_slice(&key.to_be_bytes()[..]);
            }
            Self::Blank { iri, discriminant } => {
                bytes.extend_from_slice(&iri.to_be_bytes()[..]);
                bytes.extend_from_slice(&discriminant.to_be_bytes()[..]);
            }
            Self::TypedString {
                string: first,
                iri: second,
            }
            | Self::LangString {
                string: first,
                langtag: second,
            } => {
                bytes.extend_from_slice(&first.to_be_bytes());
                bytes.extend_from_slice(&second.to_be_bytes());
            }
        }

        bytes.push(self.type_id());

        bytes
    }

    fn from_slice(bytes: &[u8]) -> Result<Self, ObjectError> {
        match bytes.len() {
            Self::SHORT_LEN => {
                let key =
                    u128::from_be_bytes(bytes[..16].try_into().expect("Length is already checked"));

                match bytes[Self::SHORT_LEN - 1] {
                    Self::IRI => Ok(Self::Iri(key)),
                    Self::STRING => Ok(Self::String(key)),
                    _ => Err(ObjectError::InvalidType),
                }
            }
            Self::MID_LEN => {
                let iri = u128::from_be_bytes(
                    bytes[0..16].try_into().expect("Length is already checked"),
                );
                let discriminant = u64::from_be_bytes(
                    bytes[16..24].try_into().expect("Length is already checked"),
                );

                Ok(Self::Blank { iri, discriminant })
            }
            Self::LONG_LEN => {
                let first =
                    u128::from_be_bytes(bytes[..16].try_into().expect("Length is already checked"));
                let second = u128::from_be_bytes(
                    bytes[16..32].try_into().expect("Length is already checked"),
                );

                match bytes[Self::LONG_LEN - 1] {
                    Self::TYPED_STRING => Ok(Self::TypedString {
                        string: first,
                        iri: second,
                    }),
                    Self::LANG_STRING => Ok(Self::LangString {
                        string: first,
                        langtag: second,
                    }),
                    _ => Err(ObjectError::InvalidType),
                }
            }
            size => Err(ObjectError::IncorrectByteLength(size)),
        }
    }

    const fn type_id(&self) -> u8 {
        match self {
            Self::Iri(_) => Self::IRI,
            Self::Blank { .. } => Self::BLANK,
            Self::String(_) => Self::STRING,
            Self::TypedString { .. } => Self::TYPED_STRING,
            Self::LangString { .. } => Self::LANG_STRING,
        }
    }

    const fn bytes_len(&self) -> usize {
        match self {
            Self::Iri(_) | Self::String(_) => Self::SHORT_LEN,
            Self::Blank { .. } => Self::MID_LEN,
            Self::TypedString { .. } | Self::LangString { .. } => Self::LONG_LEN,
        }
    }
}

impl KeyEncoding for Object {
    type Error = ObjectError;

    const LENGTH: Option<usize> = None;

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(std::borrow::Cow::Owned(self.to_bytes()))
    }

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes)
    }
}

impl<'k> Key<'k> for Object {
    const CAN_OWN_BYTES: bool = false;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        Self::from_slice(bytes.as_ref())
    }
}

impl std::fmt::Display for ObjectError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IncorrectByteLength(size) => write!(f, "Incorrect byte length: {size}"),
            Self::InvalidType => write!(f, "Invalid type byte"),
        }
    }
}

impl std::error::Error for ObjectError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::IncorrectByteLength(_) => None,
            Self::InvalidType => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Object;

    #[test]
    fn round_trip() {
        let inputs = [
            Object::Iri(u128::MAX),
            Object::Iri(0),
            Object::Iri(99),
            Object::Blank {
                iri: u128::MAX,
                discriminant: u64::MAX,
            },
            Object::Blank {
                iri: 0,
                discriminant: u64::MAX,
            },
            Object::Blank {
                iri: 99,
                discriminant: u64::MAX,
            },
            Object::String(u128::MAX),
            Object::Blank {
                iri: u128::MAX,
                discriminant: 0,
            },
            Object::Blank {
                iri: 0,
                discriminant: 0,
            },
            Object::Blank {
                iri: 99,
                discriminant: 0,
            },
            Object::Blank {
                iri: u128::MAX,
                discriminant: 99,
            },
            Object::Blank {
                iri: 0,
                discriminant: 99,
            },
            Object::Blank {
                iri: 99,
                discriminant: 99,
            },
            Object::String(u128::MAX),
            Object::String(0),
            Object::String(99),
            Object::TypedString {
                string: u128::MAX,
                iri: u128::MAX,
            },
            Object::TypedString { string: 0, iri: 0 },
            Object::TypedString {
                string: 99,
                iri: 99,
            },
            Object::TypedString {
                string: u128::MAX,
                iri: 0,
            },
            Object::TypedString {
                string: 0,
                iri: u128::MAX,
            },
            Object::LangString {
                string: u128::MAX,
                langtag: u128::MAX,
            },
            Object::LangString {
                string: 0,
                langtag: 0,
            },
            Object::LangString {
                string: 99,
                langtag: 99,
            },
            Object::LangString {
                string: u128::MAX,
                langtag: 0,
            },
            Object::LangString {
                string: 0,
                langtag: u128::MAX,
            },
        ];

        for input in inputs {
            let bytes = input.to_bytes();

            let object = Object::from_slice(&bytes).expect("Deserialize successfully");

            assert_eq!(input, object);
        }
    }
}
