use std::hash::Hash;

use bonsaidb::core::key::{Key, KeyEncoding};
use serde::de::Error as _;

#[derive(Clone, Debug)]
pub(crate) enum Iri<'a> {
    Ref(iref::Iri<'a>),
    Owned(iref::IriBuf),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize)]
#[serde(transparent)]
pub(crate) struct OwnedIri(pub(crate) Iri<'static>);

impl<'a> From<Iri<'a>> for iref::IriBuf {
    fn from(value: Iri<'a>) -> Self {
        match value {
            Iri::Owned(owned) => owned,
            Iri::Ref(borrowed) => borrowed.to_owned(),
        }
    }
}

impl<'a> From<iref::Iri<'a>> for Iri<'a> {
    fn from(value: iref::Iri<'a>) -> Self {
        Self::Ref(value)
    }
}

impl<'a> From<&'a iref::IriBuf> for Iri<'a> {
    fn from(value: &'a iref::IriBuf) -> Self {
        Self::Ref(value.as_iri())
    }
}

impl From<iref::IriBuf> for Iri<'static> {
    fn from(value: iref::IriBuf) -> Self {
        Self::Owned(value)
    }
}

impl<'a> From<&'a iref::IriBuf> for OwnedIri {
    fn from(value: &'a iref::IriBuf) -> Self {
        Self(Iri::from(value).into_owned())
    }
}

impl From<iref::IriBuf> for OwnedIri {
    fn from(value: iref::IriBuf) -> Self {
        Self(value.into())
    }
}

impl From<OwnedIri> for iref::IriBuf {
    fn from(value: OwnedIri) -> Self {
        value.0.into()
    }
}

impl<'a> Iri<'a> {
    fn into_owned(self) -> Iri<'static> {
        Iri::Owned(self.into())
    }

    fn to_bytes(&self) -> std::borrow::Cow<'_, [u8]> {
        match self {
            Self::Ref(iri) => std::borrow::Cow::Borrowed(iri.as_ref()),
            Self::Owned(iri) => std::borrow::Cow::Borrowed(iri.as_ref()),
        }
    }

    fn as_str(&self) -> &str {
        match self {
            Self::Ref(iri) => iri.as_str(),
            Self::Owned(iri) => iri.as_str(),
        }
    }
}

impl<'k> Iri<'k> {
    fn from_bytes<'e>(bytes: bonsaidb::core::key::ByteSource<'k, 'e>) -> Result<Self, IriError> {
        let iri = match bytes {
            bonsaidb::core::key::ByteSource::Owned(vec) => {
                Iri::Owned(iref::IriBuf::from_vec(vec).map_err(|(e, _)| IriError::InvalidIri(e))?)
            }
            bonsaidb::core::key::ByteSource::Ephemeral(slice) => {
                Iri::Owned(iref::IriBuf::new(slice).map_err(IriError::InvalidIri)?)
            }
            bonsaidb::core::key::ByteSource::Borrowed(slice) => {
                Iri::Ref(iref::Iri::new(slice).map_err(IriError::InvalidIri)?)
            }
        };

        Ok(iri)
    }
}

#[derive(Debug)]
pub(crate) enum IriError {
    InvalidIri(iref::Error),
}

impl<'a> KeyEncoding for Iri<'a> {
    type Error = IriError;

    const LENGTH: Option<usize> = None;

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
    }

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(self.to_bytes())
    }
}

impl<'a> KeyEncoding<OwnedIri> for Iri<'a> {
    type Error = IriError;

    const LENGTH: Option<usize> = None;

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
    }

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(self.to_bytes())
    }
}

impl<'k> Key<'k> for Iri<'k> {
    const CAN_OWN_BYTES: bool = true;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        Iri::from_bytes(bytes)
    }
}

impl KeyEncoding for OwnedIri {
    type Error = IriError;

    const LENGTH: Option<usize> = None;

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
    }

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(self.0.to_bytes())
    }
}

impl<'k> Key<'k> for OwnedIri {
    const CAN_OWN_BYTES: bool = true;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        Iri::from_bytes(bytes).map(Iri::into_owned).map(Self)
    }
}

impl std::fmt::Display for IriError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidIri(_) => write!(f, "Invalid IRI from bytes"),
        }
    }
}

impl std::error::Error for IriError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::InvalidIri(e) => Some(e),
        }
    }
}

impl<'a> PartialEq for Iri<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.as_str().eq(other.as_str())
    }
}

impl<'a> Eq for Iri<'a> {}

impl<'a> PartialOrd for Iri<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.as_str().partial_cmp(other.as_str())
    }
}

impl<'a> Ord for Iri<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.as_str().cmp(other.as_str())
    }
}

impl<'a> Hash for Iri<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.as_str().hash(state)
    }
}

impl<'a> serde::Serialize for Iri<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}

impl<'de> serde::Deserialize<'de> for Iri<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(serde::Deserialize)]
        #[serde(untagged)]
        enum Either<'a> {
            Str(&'a str),
            String(String),
        }

        match Either::deserialize(deserializer)? {
            Either::Str(s) => iref::Iri::new(s).map_err(D::Error::custom).map(Self::Ref),
            Either::String(s) => iref::IriBuf::new(&s)
                .map_err(D::Error::custom)
                .map(Self::Owned),
        }
    }
}

impl<'de> serde::Deserialize<'de> for OwnedIri {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        Iri::deserialize(deserializer)
            .map(Iri::into_owned)
            .map(Self)
    }
}

#[cfg(test)]
mod tests {
    use super::Iri;

    #[test]
    fn round_trip() {
        let inputs = [
            Iri::Owned(
                iref::IriBuf::new("https://masto.asonix.dog/users/asonix").expect("Iri is valid"),
            ),
            Iri::Ref(
                iref::Iri::new("https://masto.asonix.dog/users/asonix").expect("Iri is valid"),
            ),
        ];

        for input in inputs {
            let bytes = input.to_bytes();

            let iri = Iri::from_bytes(bytes.into()).expect("Parsed Iri");

            assert_eq!(input, iri);
        }
    }
}
