use std::hash::Hash;

use bonsaidb::core::key::{Key, KeyEncoding};
use serde::de::Error as _;

#[derive(Clone, Debug)]
pub(crate) enum LanguageTag<'a> {
    Ref(langtag::LanguageTag<'a>),
    Owned(langtag::LanguageTagBuf),
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize)]
#[serde(transparent)]
pub(crate) struct OwnedLanguageTag(pub(crate) LanguageTag<'static>);

impl<'a> From<LanguageTag<'a>> for langtag::LanguageTagBuf {
    fn from(value: LanguageTag<'a>) -> Self {
        match value {
            LanguageTag::Owned(owned) => owned,
            LanguageTag::Ref(borrowed) => {
                langtag::LanguageTagBuf::parse_copy(borrowed.as_bytes()).expect("Already validated")
            }
        }
    }
}

impl<'a> From<langtag::LanguageTag<'a>> for LanguageTag<'a> {
    fn from(value: langtag::LanguageTag<'a>) -> Self {
        Self::Ref(value)
    }
}

impl<'a> From<&'a langtag::LanguageTagBuf> for LanguageTag<'a> {
    fn from(value: &'a langtag::LanguageTagBuf) -> Self {
        Self::Ref(value.as_ref())
    }
}

impl From<langtag::LanguageTagBuf> for LanguageTag<'static> {
    fn from(value: langtag::LanguageTagBuf) -> Self {
        Self::Owned(value)
    }
}

impl<'a> From<&'a langtag::LanguageTagBuf> for OwnedLanguageTag {
    fn from(value: &'a langtag::LanguageTagBuf) -> Self {
        Self(LanguageTag::from(value).into_owned())
    }
}

impl From<langtag::LanguageTagBuf> for OwnedLanguageTag {
    fn from(value: langtag::LanguageTagBuf) -> Self {
        Self(value.into())
    }
}

impl From<OwnedLanguageTag> for langtag::LanguageTagBuf {
    fn from(value: OwnedLanguageTag) -> Self {
        value.0.into()
    }
}

impl<'a> LanguageTag<'a> {
    fn into_owned(self) -> LanguageTag<'static> {
        LanguageTag::Owned(self.into())
    }

    fn to_bytes(&self) -> std::borrow::Cow<'_, [u8]> {
        match self {
            Self::Ref(langtag) => std::borrow::Cow::Borrowed(langtag.as_bytes()),
            Self::Owned(langtag) => std::borrow::Cow::Borrowed(langtag.as_bytes()),
        }
    }

    fn as_str(&self) -> &str {
        match self {
            Self::Ref(langtag) => langtag.as_str(),
            Self::Owned(langtag) => langtag.as_str(),
        }
    }
}

impl<'k> LanguageTag<'k> {
    fn from_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, LanguageTagError> {
        let langtag = match bytes {
            bonsaidb::core::key::ByteSource::Owned(vec) => LanguageTag::Owned(
                langtag::LanguageTagBuf::new(vec)
                    .map_err(|(e, _)| LanguageTagError::InvalidLanguageTag(e))?,
            ),
            bonsaidb::core::key::ByteSource::Ephemeral(slice) => LanguageTag::Owned(
                langtag::LanguageTagBuf::parse_copy(slice)
                    .map_err(LanguageTagError::InvalidLanguageTag)?,
            ),
            bonsaidb::core::key::ByteSource::Borrowed(slice) => LanguageTag::Ref(
                langtag::LanguageTag::parse(slice).map_err(LanguageTagError::InvalidLanguageTag)?,
            ),
        };

        Ok(langtag)
    }
}

#[derive(Debug)]
pub(crate) enum LanguageTagError {
    InvalidLanguageTag(langtag::Error),
}

impl<'a> KeyEncoding for LanguageTag<'a> {
    type Error = LanguageTagError;

    const LENGTH: Option<usize> = None;

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
    }

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(self.to_bytes())
    }
}

impl<'a> KeyEncoding<OwnedLanguageTag> for LanguageTag<'a> {
    type Error = LanguageTagError;

    const LENGTH: Option<usize> = None;

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
    }

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(self.to_bytes())
    }
}

impl<'k> Key<'k> for LanguageTag<'k> {
    const CAN_OWN_BYTES: bool = true;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        LanguageTag::from_bytes(bytes)
    }
}

impl KeyEncoding for OwnedLanguageTag {
    type Error = LanguageTagError;

    const LENGTH: Option<usize> = None;

    fn describe<Visitor>(visitor: &mut Visitor)
    where
        Visitor: bonsaidb::core::key::KeyVisitor,
    {
        visitor.visit_type(bonsaidb::core::key::KeyKind::Bytes);
    }

    fn as_ord_bytes(&self) -> Result<std::borrow::Cow<'_, [u8]>, Self::Error> {
        Ok(self.0.to_bytes())
    }
}

impl<'k> Key<'k> for OwnedLanguageTag {
    const CAN_OWN_BYTES: bool = true;

    fn from_ord_bytes<'e>(
        bytes: bonsaidb::core::key::ByteSource<'k, 'e>,
    ) -> Result<Self, Self::Error> {
        LanguageTag::from_bytes(bytes)
            .map(LanguageTag::into_owned)
            .map(Self)
    }
}

impl std::fmt::Display for LanguageTagError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidLanguageTag(e) => write!(f, "Invalid langtag from bytes: {e}"),
        }
    }
}

impl std::error::Error for LanguageTagError {}

impl<'a> PartialEq for LanguageTag<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.as_str().eq(other.as_str())
    }
}

impl<'a> Eq for LanguageTag<'a> {}

impl<'a> PartialOrd for LanguageTag<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.as_str().partial_cmp(other.as_str())
    }
}

impl<'a> Ord for LanguageTag<'a> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.as_str().cmp(other.as_str())
    }
}

impl<'a> Hash for LanguageTag<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.as_str().hash(state)
    }
}

impl<'a> serde::Serialize for LanguageTag<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_str())
    }
}

impl<'de> serde::Deserialize<'de> for LanguageTag<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(serde::Deserialize)]
        #[serde(untagged)]
        enum Either<'a> {
            Str(&'a str),
            String(String),
        }

        match Either::deserialize(deserializer)? {
            Either::Str(s) => langtag::LanguageTag::parse(s)
                .map_err(D::Error::custom)
                .map(Self::Ref),
            Either::String(s) => langtag::LanguageTagBuf::parse_copy(&s)
                .map_err(D::Error::custom)
                .map(Self::Owned),
        }
    }
}

impl<'de> serde::Deserialize<'de> for OwnedLanguageTag {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        LanguageTag::deserialize(deserializer)
            .map(LanguageTag::into_owned)
            .map(Self)
    }
}

#[cfg(test)]
mod tests {
    use super::LanguageTag;

    #[test]
    fn round_trip() {
        let inputs = [
            LanguageTag::Owned(
                langtag::LanguageTagBuf::parse_copy(b"en-US").expect("LanguageTag is valid"),
            ),
            LanguageTag::Ref(langtag::LanguageTag::parse(b"en-US").expect("LanguageTag is valid")),
        ];

        for input in inputs {
            let bytes = input.to_bytes();

            let langtag = LanguageTag::from_bytes(bytes.into()).expect("Parsed LanguageTag");

            assert_eq!(input, langtag);
        }
    }
}
