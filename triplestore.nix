{ lib
, nixosTests
, rustPlatform
}:

rustPlatform.buildRustPackage {
  pname = "triplestore";
  version = "0.1.0";
  src = ./.;
  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "bonsaidb-0.4.0" = "qdwjwHlUxzyWXx7Crnar7zGhBAaWJkFa+hgvMzOuibc=";
    };
  };

  passthru.tests = { inherit (nixosTests) triplestore; };

  meta = {
    description = "A triplestore in rust";
    homepage = "https://git.asonix.dog/asonix/triplestore";
    license = [ lib.licenses.agpl3Plus ];
  };
}
