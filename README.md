# triplestore

RDF is a way to represent data that has many relationships
for example:

Say we have a post on mast of don by user 6my at yiff.life
```
{
    "@context": [
        "https://activitystreams namepsace url",
        "https://security extension url here",
        {
            "masto specific": "keys and values"
        }
    ],
    "id": "https://yiff.life/users/6my",
    "type": "Person", // unforutnately not "Cat", maybe we can add that extension sometime
    "displayName": "Angy Cat",
    "shortname": "@6my",
    "followers": "https://yiff.life/users/6my/followers"
    "publicKey": {
        "id": "https://yiff.life/users/6my#main-key",
        "owner": "https://yiff.life/users/6my",
        "publicKeyPem": "asdfasdfasdfasdfasdfasdfasfdasdfasdfasdfasdfasdfasdf"
    },
    "attachment": [
        {
            "type": "PropertyValue",
            "name": "Pronouns",
            "value": "he/him"
        },
        {
            "type": "PropertyValue",
            "name": "kofi",
            "value": "hywecp"
        }
    ]
}
```
this is "Json Linked Data"

```
https://yiff.life/users/6my,          https://activitystreams namespace url#type,        Person
https://yiff.life/users/6my,          https://activitystreams namespace url#displayName, Angy Cat
https://yiff.life/users/6my,          https://activitystreams namespace url#shortname,   @6my
https://yiff.life/users/6my,          https://activitystreams namespace url#followers,   https://yiff.life/users/6my/followers
https://yiff.life/users/6my,          https://security extension url here#publicKey,     https://yiff.life/users/6my#main-key
https://yiff.life/users/6my,          https://security extension url here#attachment,    _:a
https://yiff.life/users/6my,          https://security extension url here#attachment,    _:b
https://yiff.life/users/6my#main-key  https://security extension url here#owner,         https://yiff.life/user/6my
https://yiff.life/users/6my#main-key  https://security extension url here#publicKeyPem,  asdfasdfasdfasdfasdfasdfasfdasdfasdfasdfasdfasdfasdf
https://yiff.life/users/6my/followers https://activitystreams namespace url#items,       https://masto.asonix.dog/users/asonix
https://yiff.life/users/6my/followers https://activitystreams namespace url#items,       https://casually.cat/users/casuallynoted
_:a                                   https://schema.org/xml#type                        https://schema.org/xml#PropertyValue
_:a                                   https://schema.org/xml#name                        Pronouns
_:a                                   https://schema.org/xml#value                       he/him
_:b                                   https://schema.org/xml#type                        PropertyValue
_:b                                   https://schema.org/xml#name                        kofi
_:b                                   https://schema.org/xml#value                       hywecp
```
