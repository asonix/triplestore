{
  description = "Triplestore!";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
      {
        devShells = {
          default = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [ cargo cargo-outdated cargo-zigbuild clippy gcc rnix-lsp rust-analyzer rustc rustfmt ];
          };

          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
        };

        packages.default = pkgs.callPackage ./triplestore.nix {};

        formatter = pkgs.nixpkgs-fmt;
      });
}
